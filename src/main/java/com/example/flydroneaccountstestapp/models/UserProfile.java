package com.example.flydroneaccountstestapp.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;


import java.time.LocalDate;

@JsonPropertyOrder({"id", "surName", "name", "middleName", "birthDate"})
public class UserProfile {
    @Nullable
    @Schema( name = "Идентификатор ",description = "Идентификатор профиля пользователя", example = "1" , required = false)
    private long Id;
    @Schema(description = "Фамилия пользователя", example = "Иванов" , required = true)

    private String SurName;
    @Schema(description = "Имя пользователя", example = "Иван" , required = true)
    private String Name;
    @Schema(description = "Отчество пользователя", example = "Иванович" , required = false)
    private String MiddleName;
    @Schema(description = "Дата рождения пользователя", example = "2004-10-20" , required = true)
    private LocalDate BirthDate;

    public static UserProfile of(long Id, String Surname, String Name, String MiddleName, LocalDate BirthDate){
        UserProfile up = new UserProfile();
        up.setId(Id);
        up.setSurName(Surname);
        up.setName(Name);
        up.setMiddleName(MiddleName);
        up.setBirthDate(BirthDate);
        return up;
    }
    @Nullable
    public void setId(long Id){
        this.Id=Id;
    }
    @NonNull
    public void setSurName(String surName){
        this.SurName=surName;
    }
    @NonNull
    public void setName(String Name){
        this.Name=Name;
    }
    @Nullable
    public void setMiddleName(String MiddleName){
        this.MiddleName=MiddleName;
    }
    @NonNull
    public void  setBirthDate(LocalDate BirthDate){
        this.BirthDate = BirthDate;
    }
    @Nullable
    public long getId() {
        return Id;
    }
    @NonNull
    public String getName(){
        return Name;
    }
    @NonNull
    public String getSurName(){
        return SurName;
    }
    @Nullable
    public String getMiddleName(){
        return MiddleName;
    }
    @NonNull
    public LocalDate getBirthDate(){
        return BirthDate;
    }
    @Override
    public String toString() {
        return "User[ id=" + Id
                + ", surname=" + SurName
                + ", name=" + Name
                + ", middlename=" + MiddleName
                + ", birthdate=" + BirthDate +"]";
    }
}
