package com.example.flydroneaccountstestapp.repositories;

import com.example.flydroneaccountstestapp.repositories.interfaces.AvatarRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class AvatarRepositoryDB implements AvatarRepository {
    private JdbcTemplate jdbcTemplate;

    public AvatarRepositoryDB(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    public void insertProfileAvatar(long id, byte[] byteImage){
        jdbcTemplate.update("INSERT INTO \"ProfileAvatar\"( \"Image\", \"UserId\") VALUES ( ?, ?)", byteImage, id);
    }
    public byte[] selectProfileAvatar(long id){
        try{
            return jdbcTemplate.queryForObject("select \"Image\" from \"ProfileAvatar\" WHERE \"UserId\" = ?", byte[].class, id);
        }
        catch (EmptyResultDataAccessException e){
            return null;
        }
    }
    public void deleteProfileAvatar(long id){
        jdbcTemplate.update("DELETE FROM \"ProfileAvatar\" WHERE \"UserId\" = ?", id);
    }
    public void updateProfileAvatar(long id, byte[] byteImage){
        jdbcTemplate.update("UPDATE \"ProfileAvatar\" SET \"Image\"=? WHERE \"UserId\"=? ", byteImage, id);
    }
}
