package com.example.flydroneaccountstestapp.repositories;

import com.example.flydroneaccountstestapp.models.UserProfile;
import com.example.flydroneaccountstestapp.repositories.interfaces.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;
@Component
public class UserProfileRepositoryDB implements UserProfileRepository {
    private JdbcTemplate jdbcTemplate;
    UserProfileRepositoryDB(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    public UserProfile selectUser(long id) {
        try {
            UserProfile selectedProfile = jdbcTemplate.queryForObject("SELECT * FROM \"UserProfile\" where \"ID\" = ?",
                    (rs,row) -> mapRowToUserProfile(rs), id);
            return selectedProfile;
        }
        catch (EmptyResultDataAccessException e){
            return null;
        }
    }
    public long insertUser(UserProfile addableUser){
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(
                        "INSERT INTO \"UserProfile\"(\"Name\", \"Surname\", \"MiddleName\", \"BirthDay\") VALUES (?, ?, ?, ?)",
                        new String[] { "ID" });
                ps.setString(1, addableUser.getName());
                ps.setString(2, addableUser.getSurName());
                ps.setString(3, addableUser.getMiddleName());
                ps.setDate(4, Date.valueOf(addableUser.getBirthDate()));
                return ps;
            }
        }, keyHolder);
        return keyHolder.getKey().longValue();
    }
    public long updateUser(UserProfile editableUser){
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(
                        "UPDATE \"UserProfile\" SET \"Name\"=?, \"Surname\"=?, \"MiddleName\"=?, \"BirthDay\"=? WHERE \"ID\"=?",
                        new String[] { "ID" });
                ps.setString(1, editableUser.getName());
                ps.setString(2, editableUser.getSurName());
                ps.setString(3, editableUser.getMiddleName());
                ps.setDate(4, Date.valueOf(editableUser.getBirthDate()));
                ps.setLong(5, editableUser.getId());
                return ps;
            }
        }, keyHolder);
        return keyHolder.getKey().longValue();
    }
    public UserProfile mapRowToUserProfile(ResultSet rs) throws SQLException {
        UserProfile userProfile = UserProfile.of(
                rs.getLong("ID"),
                rs.getString("Surname"),
                rs.getString("Name"),
                rs.getString("MiddleName"),
                rs.getDate("BirthDay").toLocalDate());
        return userProfile;
    }
    public void deleteUserProfile(long id){
        jdbcTemplate.update("DELETE FROM \"UserProfile\" WHERE \"ID\" = ?;", id);
    }
}
