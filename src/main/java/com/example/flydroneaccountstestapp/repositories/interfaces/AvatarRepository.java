package com.example.flydroneaccountstestapp.repositories.interfaces;

public interface AvatarRepository {
    public void insertProfileAvatar(long id, byte[] byteImage);
    public byte[] selectProfileAvatar(long id);
    public void deleteProfileAvatar(long id);
    public void updateProfileAvatar(long id, byte[] byteImage);
}
