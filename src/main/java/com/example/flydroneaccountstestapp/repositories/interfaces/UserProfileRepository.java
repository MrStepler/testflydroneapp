package com.example.flydroneaccountstestapp.repositories.interfaces;

import com.example.flydroneaccountstestapp.models.UserProfile;

public interface UserProfileRepository {
    public UserProfile selectUser(long id);
    public long insertUser(UserProfile addableUser);
    public long updateUser(UserProfile editableUser);
    public void deleteUserProfile(long id);
}
