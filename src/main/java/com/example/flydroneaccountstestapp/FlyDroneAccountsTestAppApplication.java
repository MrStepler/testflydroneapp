package com.example.flydroneaccountstestapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlyDroneAccountsTestAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlyDroneAccountsTestAppApplication.class, args);
    }

}
