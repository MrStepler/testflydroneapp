package com.example.flydroneaccountstestapp.services;

import com.example.flydroneaccountstestapp.repositories.interfaces.UserProfileRepository;
import com.example.flydroneaccountstestapp.services.interfaces.UserService;
import com.example.flydroneaccountstestapp.models.UserProfile;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {
    UserProfileRepository profileRepository;
    public UserServiceImpl(UserProfileRepository repository){
        this.profileRepository = repository;
    }
    @Override
    public @Nullable UserProfile getUserById(long id) {
        return profileRepository.selectUser(id);
    }

    public long editOrAddUser(UserProfile addableOrEditableUser){
        if(addableOrEditableUser.getId() == 0){
            return profileRepository.insertUser(addableOrEditableUser);
        }
        else {
            return profileRepository.updateUser(addableOrEditableUser);
        }
    }
    public void deleteUser(long id){
        profileRepository.deleteUserProfile(id);
    }
}
