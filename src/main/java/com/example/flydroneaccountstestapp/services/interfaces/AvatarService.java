package com.example.flydroneaccountstestapp.services.interfaces;

import org.springframework.lang.Nullable;

public interface AvatarService {
    public void addAvatarToUser(long id, byte[] byteImage, boolean rewrite);
    public @Nullable byte[] getUserAvatar(long id);
    public void deleteUserAvatar(long id);

}
