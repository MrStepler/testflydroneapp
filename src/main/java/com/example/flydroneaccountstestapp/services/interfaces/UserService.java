package com.example.flydroneaccountstestapp.services.interfaces;

import com.example.flydroneaccountstestapp.models.UserProfile;

public interface UserService {
    public UserProfile getUserById(long id);
    public long editOrAddUser(UserProfile addableUser);
    public void deleteUser(long id);
}
