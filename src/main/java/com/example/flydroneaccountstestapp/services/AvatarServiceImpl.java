package com.example.flydroneaccountstestapp.services;

import com.example.flydroneaccountstestapp.repositories.interfaces.AvatarRepository;
import com.example.flydroneaccountstestapp.services.interfaces.AvatarService;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class AvatarServiceImpl implements AvatarService {
    AvatarRepository avatarRepository;
    public AvatarServiceImpl(AvatarRepository avatarRepository){
        this.avatarRepository = avatarRepository;
    }
    public void addAvatarToUser(long id, byte[] byteImage, boolean rewrite){
        if(rewrite){
            avatarRepository.updateProfileAvatar(id,byteImage);
        }
        else{
            avatarRepository.insertProfileAvatar(id,byteImage);
        }
    }
    public @Nullable byte[] getUserAvatar(long id){
        return avatarRepository.selectProfileAvatar(id);
    }
    public void deleteUserAvatar(long id){
        avatarRepository.deleteProfileAvatar(id);
    }


}
