package com.example.flydroneaccountstestapp.controllers;

import com.example.flydroneaccountstestapp.services.interfaces.AvatarService;
import com.example.flydroneaccountstestapp.services.interfaces.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class AvatarController {
    private final AvatarService avatarService;
    private final UserService userService;
    public AvatarController(AvatarService avatarService, UserService userService){
        this.avatarService = avatarService;
        this.userService = userService;
    }


    @Operation(summary = "Получение файла аватарки профиля")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Файл изображения получен", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Профиль пользователя, с указанным Id не найден/Аватарка не установлена", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "400", description = "Неверный формат идентификатора профиля", content = @Content(schema = @Schema(hidden = true)))
    })
    @RequestMapping(path = "/avatar/{profileId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getProfileAvatar(@Parameter(description = "Идентификатор профиля") @PathVariable long profileId) {
        if(profileId <= 0){
            return ResponseEntity.badRequest().build();
        }
        if(userService.getUserById(profileId) == null){
            return ResponseEntity.notFound().build();
        }
        var gottenAvatarObject = avatarService.getUserAvatar(profileId);
        if(gottenAvatarObject == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(gottenAvatarObject);
    }

    @Operation(summary = "Загрузка аватарки в профиль пользователя, с указанием перезаписи", description = "Если в профиле уже установлена аватарка, то rewrite=true, иначе false;")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Аватарка профиля установлена", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Профиль пользователя, с указанным Id не найден", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "400", description = "Неверный формат запроса", content = @Content(schema = @Schema(hidden = true)))
    })
    @RequestMapping(path = "/avatar/{profileId}",method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Void> uploadProfileAvatar(boolean rewrite,
                                                    @Parameter(description = "Идентификатор профиля") @PathVariable long profileId,
                                                    @Parameter(description = "Файл изображения") @RequestPart("file") MultipartFile picture)
            throws MaxUploadSizeExceededException {
        if(profileId <= 0){
            return ResponseEntity.badRequest().build();
        }
        if(userService.getUserById(profileId) == null){
            return ResponseEntity.notFound().build();
        }
        if(picture != null){
            return ResponseEntity.badRequest().build();
        }
        if(picture.getSize() > 1024*1024*2){
            return ResponseEntity.badRequest().build();
        }
        if(!isPicture(picture)){
            return ResponseEntity.badRequest().build();
        }
        //Блок проверки на перезапись
        if(!rewrite && avatarService.getUserAvatar(profileId) != null){
            return ResponseEntity.badRequest().build();
        }
        if(rewrite && avatarService.getUserAvatar(profileId) == null){
            return ResponseEntity.badRequest().build();
        }
        //-------------------------------
        try{
            avatarService.addAvatarToUser(profileId,picture.getBytes(), rewrite);
        }
        catch (IOException e){
            return ResponseEntity.unprocessableEntity().build();
        }
        return ResponseEntity.ok().build();
    }
    @Operation(summary = "Удаления аватарки профиля пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Аватарка профиля пользователя удалена", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Профиль пользователя, с указанным Id не найден/Аватарка профиля не установлена", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "400", description = "Неверный формат данных", content = @Content(schema = @Schema(hidden = true)))
    })
    @RequestMapping(path = "/avatar/{profileId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteProfileAvatar(@Parameter(description = "Идентификатор профиля") @PathVariable long profileId){
        if(profileId <= 0){
            return ResponseEntity.badRequest().build();
        }
        if(userService.getUserById(profileId) == null){
            return ResponseEntity.notFound().build();
        }
        if(avatarService.getUserAvatar(profileId) == null){
            return ResponseEntity.notFound().build();
        }
        avatarService.deleteUserAvatar(profileId);
        return ResponseEntity.ok().build();
    }
    //Статичный метод проверки является ли файл формата .jpg или .png
    public static boolean isPicture(MultipartFile fileToCheck){
        if(fileToCheck.getOriginalFilename().toLowerCase().endsWith(".jpg") ){
            return true;
        }
        if(fileToCheck.getOriginalFilename().toLowerCase().endsWith(".png") ){
            return true;
        }
        return false;
    }
}
