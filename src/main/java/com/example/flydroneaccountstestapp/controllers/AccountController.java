package com.example.flydroneaccountstestapp.controllers;

import com.example.flydroneaccountstestapp.models.*;
import com.example.flydroneaccountstestapp.services.interfaces.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.*;
import io.swagger.v3.oas.annotations.responses.*;
import jakarta.annotation.Nullable;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;


@RestController()
@OpenAPIDefinition(
        info = @Info(
                title = "User profile API",
                version = "1.0"
        )
)

public class AccountController {
    private final UserService userService;
    private final AvatarService avatarService;
    public AccountController(UserService userService, AvatarService avatarService){
        this.userService = userService;
        this.avatarService = avatarService;
    }

    @GetMapping("/profile/{id}")
    @Operation(summary = "Получение информации о пользователе, по ID профиля")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь получен", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UserProfile.class))),
            @ApiResponse(responseCode = "404", description = "Пользователь не найден", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "400", description = "Неверный формат поля Id", content = @Content(schema = @Schema(hidden = true)))
    })
    public ResponseEntity<UserProfile> getUser(
            @Parameter(description = "Идентификатор профиля") @PathVariable long id){
        if(id <= 0){
            return ResponseEntity.badRequest().build();
        }
        @Nullable var gottenUser = userService.getUserById(id);
        if(gottenUser == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(gottenUser);
    }
    @PostMapping("/profile")
    @Operation(summary = "Добавление/изменение информации в профиле пользователе")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Профиль пользователя изменен/создан", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Long.class))),
            @ApiResponse(responseCode = "404", description = "Профиль пользователя, с указанным Id не найден", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "400", description = "Неверный формат данных", content = @Content(schema = @Schema(hidden = true)))
    })
    public ResponseEntity<Long> editOrAddUser(@RequestBody UserProfile addableEditableUser){
        if(!isValidUser(addableEditableUser)){
            return ResponseEntity.badRequest().build();
        }
        if(addableEditableUser.getId() > 0 && userService.getUserById(addableEditableUser.getId()) == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(userService.editOrAddUser(addableEditableUser));
    }
    @DeleteMapping("/profile/{id}")
    @Operation(summary = "Удаление профиля пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Профиль пользователя удален", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Профиль пользователя, с указанным Id не найден", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "400", description = "Неверный формат данных", content = @Content(schema = @Schema(hidden = true)))
    })
    public ResponseEntity<Void> deleteProfile(@Parameter(description = "Идентификатор профиля") @PathVariable long id){
        if(id <= 0){
            return ResponseEntity.badRequest().build();
        }
        if( userService.getUserById(id) == null){
            return ResponseEntity.notFound().build();
        }
        if(avatarService.getUserAvatar(id) != null){
            avatarService.deleteUserAvatar(id);
        }
        userService.deleteUser(id);
        return ResponseEntity.ok().build();
    }
    @Operation(
            summary = "Добавление/изменение профиля пользователя вместе с картинкой",
            description = "JSON файл передается в формате строки; изображение передается файлом; допустимый формат файла jpg, png. При отсутствии изображения создает профиль без аватарки"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Профиль пользователя добавлен/изменен", content = @Content(mediaType = "application/json", schema = @Schema(implementation = Long.class))),
            @ApiResponse(responseCode = "404", description = "Профиль пользователя, с указанным Id не найден", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "400", description = "Неверный формат данных", content = @Content(schema = @Schema(hidden = true)))
    })
    @RequestMapping(path = "/fullprofile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Long> editOrAddFullUser(@Parameter(description = "Строковый JSON") @RequestPart(value = "user", required = true) String jsonUserProfile,
                                                  @Parameter(description = "Файл изображения") @RequestPart(value = "file", required = false) MultipartFile file)
    throws MaxUploadSizeExceededException {
       if(file != null){
           if(file.getSize() > 1024*1024*2){
               return ResponseEntity.badRequest().build();
           }
       }
        //Блок маппинга JSON в объекты
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        UserProfile userProfile;
        try{
            userProfile = objectMapper.readValue(jsonUserProfile, UserProfile.class);
        }
        catch (Exception e){
            return ResponseEntity.badRequest().build();
        }
        //----------------------------
        if(!isValidUser(userProfile)){
            return ResponseEntity.badRequest().build();
        }
        if(userProfile.getId() > 0 && userService.getUserById(userProfile.getId()) == null){
            return ResponseEntity.notFound().build();
        }
        long createdUserId = userService.editOrAddUser(userProfile);
        if(file != null){
            if(!AvatarController.isPicture(file)){
                return ResponseEntity.badRequest().build();
            }
        }
        //Блок проверки на перезапись аватарки
        if (userProfile.getId() > 0 && userService.getUserById(userProfile.getId()) != null && file != null){
            try {
                avatarService.addAvatarToUser(userProfile.getId(), file.getBytes(), true );
            }
            catch (IOException e){
                return ResponseEntity.badRequest().build();
            }
        } else if (file != null) {
            try {
                avatarService.addAvatarToUser(createdUserId, file.getBytes(), false );
            }
            catch (Exception e){
                return ResponseEntity.badRequest().build();
            }
        }
        //--------------------------------------
        return ResponseEntity.ok(createdUserId);
    }
    private boolean isValidUser(UserProfile checkableUser){
        if(checkableUser.getId() < 0){
            return false;
        }
        if (checkableUser.getBirthDate() == null){
            return false;
        }
        if (checkableUser.getBirthDate().toString().isEmpty()){
            return false;
        }
        int age = LocalDate.now().getYear() - checkableUser.getBirthDate().getYear() - 1;
        if(checkableUser.getBirthDate().getMonth().getValue() > LocalDate.now().getMonth().getValue()){
            if(checkableUser.getBirthDate().getDayOfMonth() > LocalDate.now().getDayOfMonth()){
                age++;
            }
        }
        if(age < 18){
            return false;
        }
        if(checkableUser.getSurName() == null){
            return false;
        }
        if(checkableUser.getSurName().isEmpty() || checkableUser.getSurName().length() < 2){
            return false;
        }
        if(checkableUser.getName() == null){
            return false;
        }
        if(checkableUser.getName().isEmpty() || checkableUser.getName().length() < 2){
            return false;
        }
        //Минимальный размер отчества 5 символов
        if(checkableUser.getMiddleName() != null ){
            if(!checkableUser.getMiddleName().isEmpty()){
                if(checkableUser.getMiddleName().length() <5){
                    return false;
                }
            }
        }
        return true;
    }
}
